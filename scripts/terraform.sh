#!/bin/bash

echo "Initializing Terraform"
terraform init

echo "Generate plan"
terraform plan -out="/go/src/github.com/hashicorp/terraform/apply.pln"

echo "Apply plan"
terraform apply /go/src/github.com/hashicorp/terraform/apply.pln
