#!/bin/bash

# Create infrastructure using terraform
terraform_res=$(docker-compose up terraform)
echo "$terraform_res"
outputs=$(echo "$terraform_res" | grep -A 7 --color 'Outputs')
dns_name=$(echo "$outputs" | grep grep 'dns_name = [[:lower:]]')

# Install packages and deploy app on created VPS
docker-compose up --build ansible

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "Terraform outputs:
$outputs"
echo "====================================================="
echo "The site will be available by the next URL in a few minutes: $dns_name"
