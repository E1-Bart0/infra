output "key_name" {
  value = aws_key_pair.this.key_name
}
output "private_key_path" {
  value = "${var.ssh_key_path}/${aws_key_pair.this.key_name}"
}
output "public_key_path" {
  value = "${var.ssh_key_path}/${aws_key_pair.this.key_name}.pub"
}
