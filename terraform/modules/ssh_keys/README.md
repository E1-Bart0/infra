###  SSH Key-Pair generation

Variables:
- ssh_key_name: Name of the created key-pair: _(id_rsa)_
- ssh_key_path: Path to folder, where to store private and public keys: _(base-dir/keys)_

This module will generate a Key-Pair and store it into Volume: _/keys_. This volume is mounted with _base-dir/keys/_
