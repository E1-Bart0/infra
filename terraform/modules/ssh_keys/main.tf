# Generating a new SSH key
resource "tls_private_key" "this" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "this" {
  key_name   = var.ssh_key_name       # Create "id_rsa" to AWS!!
  public_key = tls_private_key.this.public_key_openssh

  provisioner "local-exec" {
    # Create "id_rsa" in docker volume: /keys/id_rsa and /keys/id_rsa.pub!!
    command = "echo '${tls_private_key.this.private_key_pem}' > ${var.ssh_key_path}/${var.ssh_key_name} && chmod 600 ${var.ssh_key_path}/${var.ssh_key_name} && echo '${tls_private_key.this.public_key_openssh}' > ${var.ssh_key_path}/${var.ssh_key_name}.pub"
  }
}
