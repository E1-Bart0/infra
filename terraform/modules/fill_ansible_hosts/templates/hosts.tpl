[${section_name}]
%{ for instance_ip in instances_ips ~}
${section_name} ansible_host=${instance_ip} ansible_user=${instance_user} ansible_ssh_private_key_file=${ssh_path}
%{ endfor ~}
