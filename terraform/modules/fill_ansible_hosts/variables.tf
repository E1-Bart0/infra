variable "section_name" {
  type = string
}
variable "instance_user" {
  type = string
}
variable "instances_ips" {
  type = list(string)
}
variable "ssh_path" {
  type = string
}
variable "output_inventory_file" {
  type = string
}
