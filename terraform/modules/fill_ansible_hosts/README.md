### Ansible inventory generation

Variables
- section_name: Name of the hosts group: _(prod_web)_
- instance_user: Username of the created instance _(ubuntu)_
- instances_ips: Public IP address if the instances, that have been created at the module **aws_instance**
- ssh_path: Name of the created key, that defined in the module _ssh_keys_ _(id_rsa)_
- output_inventory_file: Location of the generated file: _(Volume: **/keys/hosts**, that  mounted to **project-dir/keys/hosts**)_

This module will fill template with variables and save this template into **output_inventory_file**.

Template _(./template/hosts.tpl)_:

```tpl
[${section_name}]
%{ for instance_ip in instances_ips ~}
${section_name} ansible_host=${instance_ip} ansible_user=${instance_user} ansible_ssh_private_key_file=${ssh_path}
%{ endfor ~}
```
