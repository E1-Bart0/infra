resource "local_file" "this" {
  content = templatefile("${path.module}/templates/hosts.tpl",
    {
    section_name = var.section_name
    instances_ips = var.instances_ips
    instance_user = var.instance_user
    ssh_path = var.ssh_path
    }
  )
  filename = var.output_inventory_file
}
