output "dns_name" {
  description = "DNS name of Load Balancer"
  value       = aws_elb.this.dns_name
}
