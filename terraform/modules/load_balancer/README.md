### LoadBalancer creation

Variables:
- name: Name of the LoadBalancer: _(prod_web)_
- subnets: A list with id-s of the Available zones that were defined in _prod.tf_
- security_groups: Some security rules that were defined in _prod.tf_
- autoscaling_group_id: The id of the AutoScaling Group in the module  **aws_instance**

This module will create a LoadBalancer and assign each instance that has been created with AutoScaling Group in the module **aws_instance**
