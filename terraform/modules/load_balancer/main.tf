# Creating a LoadBalancer
resource "aws_elb" "this" {
  name            = var.name
  subnets         = var.subnets
  security_groups = var.security_groups

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  tags = {
    "Terraform" : "true"
    "Skillbox" : "true"
  }
}

# Add New Servers to Load Balancer
resource "aws_autoscaling_attachment" "this" {
  autoscaling_group_name = var.autoscaling_group_id
  elb                    = aws_elb.this.id
}
