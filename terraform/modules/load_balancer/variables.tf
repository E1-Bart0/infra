variable "name" {
  type = string
}
variable "subnets" {
  type = list(string)
}
variable "security_groups" {
  type = list(string)
}
variable "autoscaling_group_id" {
  type = string
}
