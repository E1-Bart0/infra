variable "web_instance_id" {
  type = string
}
variable "web_desired_capacity" {
  type = number
}
variable "web_min_size" {
  type = number
}
variable "web_max_size" {
  type = number
}
variable "subnets" {
  type = list(string)
}
variable "security_groups" {
  type = list(string)
}
variable "name" {
  type = string
}
variable "ssh_key" {
  type = string
}
