# Search Ubuntu image
data "aws_ami" "this" {
  most_recent = true
  owners      = ["099720109477"]  # Canonical
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Defining an AutoScaling Group for creating instances
resource "aws_launch_configuration" "this" {
  name_prefix   = var.name
  image_id      = data.aws_ami.this.id
  instance_type = var.web_instance_id

  security_groups = var.security_groups
  key_name        = var.ssh_key

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "this" {
  launch_configuration = aws_launch_configuration.this.id
  vpc_zone_identifier  = var.subnets

  desired_capacity = var.web_desired_capacity
  min_size         = var.web_min_size
  max_size         = var.web_max_size

  dynamic tag {
    for_each = {
      "Terraform" : "true"
      "Skillbox" : "true"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
}

data "aws_instances" "this" {
  instance_tags = {
    "Terraform" : "true"
    "Skillbox": "true"
  }

  depends_on = [
    aws_autoscaling_group.this
  ]
}
