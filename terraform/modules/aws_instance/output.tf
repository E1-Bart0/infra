output "ip_addresses" {
  description = "VPS IP Addresses"
  value = data.aws_instances.this.public_ips
}
output "autoscaling_group_id" {
  description = "id of the autoscaling group"
  value       = aws_autoscaling_group.this.id
}
