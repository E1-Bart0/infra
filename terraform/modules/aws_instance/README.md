###  AutoScaling Group and Launch Configuration creation

Variables:
- web_instance_id: EC2 instance type: _(t2.micro)_
- web_desired_capacity: Desired numbers of instances: _(1)_
- web_min_size: Minimal number of the instances: _(1)_
- web_max_size:  Maximal number of the instances: _(1)_
- subnets: A list with id-s of the Available zones that were defined in _prod.tf_
- security_groups: Some security rules that were defined in _prod.tf_
- name: Instance name will start from this name: _(prod_web)_
- ssh_key: Name of the created key, that defined in the module _ssh_keys_ _(id_rsa)_

Module has: 

1. Image Searching:

Search image of the _ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*_ in AWS.

2. Launch Configuration:
- name: **name**
- image: **Image Searching**
- instance_type: **web_instance_id**
- security_group: **security_groups**
- key_name:  **ssh_key**

3. AutoScalingGroup:
- launch_configuration: **Launch Configuration**
- vpc_zone_identifier : **subnets**
- desired_capacity: **web_desired_capacity**
- min_size: **web_min_size**
- max_size:  **web_max_size**

4 Search created instances:

Search created instances by tag:  Terraform, Skillbox.
This is needed to know the public id of the created instances.
