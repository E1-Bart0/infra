# Define variables
variable "ingress_ports" {
  type = list(number)
}
variable "whitelist" {
  type = list(string)
}
variable "available_zone_1" {
  type = string
}
variable "available_zone_2" {
  type = string
}

# AWS ES2 instance
variable "username" {
  type = string
}
variable "web_instance_id" {
  type = string
}
variable "web_desired_capacity" {
  type = number
}
variable "web_min_size" {
  type = number
}
variable "web_max_size" {
  type = number
}

# SSH
variable "ssh_key_path" {
  type = string
}
variable "ssh_key_name" {
  type = string
}

# Ansible
variable "output_inventory_file" {
  type = string
}
variable "ansible_section_name" {
  type = string
}
