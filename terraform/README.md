###  Prod.tf

AWS provider is used for creating:
- Security Group;
- VPC;
- SSH key (Key Pair);
- AutoScaling Group;
- Launch Configuration;
- 1 AWS Instance (t2.micro);
- LoadBalancer;
- Ansible inventory file.

1. Get VPC:

Used default Amazon Virtual Private Cloud (VPC)

2. Search Available Zones

Searching for default Amazon available zones. By default, us-east-2a and us-east-2b.

Used variables:
- available_zone_1: Zone 1: _(us-east-2a)_;
- available_zone_2: Zone 2: _(us-east-2b)_;

3. Create Security Group:

Creating a security group with some rules: Allow standard ssh, HTTP, and HTTPS ports inbound and everything outbound.

Used variables:
- ingress_ports: Ports that will be open for security  _([22, 80, 443])_;
- whitelist: Allow all to connect _(["0.0.0.0/0"])_;

4. Generate SSH KEY:

Using module: _(ssh_keys)_. Will generate a Key-Pair and store it into Volume: _/keys_. This volume is mounted with _./keys/_

Used variables:
- ssh_key_name: Name of the created key-pair _(id_rsa)_;
- ssh_key_path: Path to folder, where to store private and public keys _(/keys)_;

5. Create AutoScaling Group

Using module: _(aws_instance)_. Will create AutoScaling Group with Launch Configuration

Used variables:
- web_instance_id: EC2 instance type: _(t2.micro);
- web_desired_capacity: Desired numbers of instances: _(1)_;
- web_min_size: Minimal number of the instances: _(1)_;
- web_max_size Maximal number of the instances: _(1)_;

6. Create Elastic LoadBalancer

Using module: _(load_balancer)_. Will create a LoadBalancer and assign each instance that has been created with AutoScaling Group.

7. Generate Ansible inventory file

Using module: _(fill_ansible_hosts)_. Will fill template with variables and save this template into _./inventory_

Used variables:
- output_inventory_file: Location of the generated file: _(/inventory/hosts)_;
- ansible_section_name: Name of the hosts group: _(prod_web)_;
- username:  Username of the created instance _(ubuntu)_;

Outputs:
- LoadBalancer DNS name;
- Public IP of the created instance;
- Path to generated inventory file;
- Path to generated private key;
