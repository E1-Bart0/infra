ingress_ports    = [22, 80, 443]
whitelist        = ["0.0.0.0/0"]
available_zone_1 = "us-east-2a"
available_zone_2 = "us-east-2b"

username             = "ubuntu"
web_instance_id      = "t2.micro"
web_desired_capacity = 1
web_min_size         = 1
web_max_size         = 1

ssh_key_name     = "id_rsa"
ssh_key_path     = "/keys"

output_inventory_file = "/inventory/hosts"
ansible_section_name  = "prod_web"
