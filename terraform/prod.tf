# Configure the AWS Provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Get Info from .env
provider "aws" {}

resource "aws_default_vpc" "default" {}

# Getting Available Zones For Region
resource "aws_default_subnet" "default_az1" {
  availability_zone = var.available_zone_1
  tags              = {
    "Terraform" : "true"
    "Skillbox" : "true"
  }
}
resource "aws_default_subnet" "default_az2" {
  availability_zone = var.available_zone_2
  tags              = {
    "Terraform" : "true"
    "Skillbox" : "true"
  }
}

# Security Group
resource "aws_security_group" "prod_web" {
  name        = "prod_web"
  description = "Allow standard ssh, http and https ports inbound and everything outbound"

  dynamic ingress {
    for_each = var.ingress_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = var.whitelist
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.whitelist
  }

  tags = {
    "Terraform" : "true"
    "Skillbox" : "true"
  }
}

module "ssh_key" {
  source = "./modules/ssh_keys"

  ssh_key_name = var.ssh_key_name
  ssh_key_path = var.ssh_key_path
}

module "web_instances" {
  source = "./modules/aws_instance"

  name                 = "prod-web"
  web_desired_capacity = var.web_desired_capacity
  web_instance_id      = var.web_instance_id
  web_max_size         = var.web_max_size
  web_min_size         = var.web_min_size
  security_groups      = [aws_security_group.prod_web.id]
  subnets              = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
  ssh_key              = module.ssh_key.key_name
}

module "elb" {
  source = "./modules/load_balancer"

  autoscaling_group_id = module.web_instances.autoscaling_group_id
  name                 = "prod-web"
  security_groups      = [aws_security_group.prod_web.id]
  subnets              = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
}

module "generate_hosts" {
  source = "./modules/fill_ansible_hosts"

  instance_user         = var.username
  instances_ips         = module.web_instances.ip_addresses
  output_inventory_file = var.output_inventory_file
  section_name          = var.ansible_section_name
  ssh_path              = module.ssh_key.private_key_path
}

output "dns_name" {
  description = "LoadBalancer DNS name"
  value       = module.elb.dns_name
}
output "ip_addresses" {
  description = "VPS Ip Addresses"
  value       = module.web_instances.ip_addresses
}
output "ansible_hosts" {
  description = "Path to ansible inventory file"
  value       = var.output_inventory_file
}
output "ssh_keys" {
  description = "Path to ssh"
  value       = module.ssh_key.private_key_path
}
