# Keys

In the current folder, Terraform will palace-created Key-Pair for EC2 instances.
And Ansible will use these keys to connect to these instances by ssh.

By default, files:
- id_rsa _(Private key)_
- id_rsa.pub _(Public key)_
