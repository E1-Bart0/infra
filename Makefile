#!make

# Get variable from environment
include .env
export

# Work with such make commands, but not with file with the same name
.PHONY: terraform
.PHONY: ansible

deploy:
	sh ./scripts/deploy.sh

destroy:
	make terraform_destroy

terraform:
	docker run --rm -it \
		 --env-file=".env" \
		 -v ${CURDIR}/keys:/keys \
		 -v ${CURDIR}/inventory:/inventory \
		 -v ${CURDIR}/terraform:/go/src/github.com/hashicorp/terraform \
		 -w /go/src/github.com/hashicorp/terraform \
		 hashicorp/terraform:latest \
		 ${cmd}

terraform_apply:
	make terraform cmd="apply -auto-approve"

terraform_destroy:
	make terraform cmd="destroy -auto-approve"

ansible:
	docker build ansible --tag ansible
	docker run --rm -it \
		--env-file=".env" \
		 -v ${CURDIR}/keys:/keys \
		 ansible \
		 ${cmd}

ansible_apply:
	docker-compose up ansible
